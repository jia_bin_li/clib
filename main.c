#include <stdio.h>
#include "src/memory/clib_memory.h"
#include "src/test/container/hash_set/hash_set_test.c"


int main(int argc, char *argv[]) {
    printf("hello clib\n");
    clib_memory_allocator_init(CLIB_ALLOCATOR_TYPE_STANDARD);

    test_hash_set();
    return 0;
}
