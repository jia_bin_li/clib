# clib

一个好用的C语言工具库，提供了基本容器、hash库、内存库、随机库等。

## 安装

1. 下载源码
```c
git clone https://gitee.com/jia_bin_li/clib.git
```
2. 在源码目录创建build文件夹
```c
cd clib
mkdir build
```
3. 进入build目录

```c
cd build
```

4. 执行编译、安装命令
```c
cmake ..
make
sudo make install
```

## 文档目录

### 容器

- [单向链表](docs/container/single_linked_list.md)
- [双向链表](docs/container/double_linked_list.md)
- [哈希表](docs/container/hash_map.md)
- [哈希集合](docs/container/hash_set.md)
- [动态数组](docs/container/vector.md)
- [双端动态数组](docs/container/double_ended_vector.md)
- [FIFO队列](docs/container/fifo_queue.md)
- [栈](docs/container/stack.md)

### 编码

- [BASE32](docs/coded/base32.md)
- [BASE64](docs/coded/base64.md)

### 哈希计算

- [adler32](docs/hash/adler32.md)
- [ap](docs/hash/ap.md)
- [bkdr](docs/hash/bkdr.md)
- [blizzard](docs/hash/blizzard.md)
- [crc](docs/hash/crc.md)
- [djb2](docs/hash/djb2.md)
- [fnv](docs/hash/fnv.md)
- [md5](docs/hash/md5.md)
- [murmur](docs/hash/murmur.md)
- [rs](docs/hash/rs.md)
- [sdbm](docs/hash/sdbm.md)

### 随机

- [根据系统时间获得随机数](docs/random/random_num_clock.md)
- [UUID](docs/random/random_uuid.md)