
#ifndef CLIB_CLIB_HASH_BLIZZARD_H
#define CLIB_CLIB_HASH_BLIZZARD_H


#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

/**
 * blizzard hash 计算
 * @param data 数据
 * @param size 大小
 * @param seed 种子
 * @return
 */
size_t clib_hash_blizzard(clib_uint8_t const *data, size_t size, size_t seed);


/**
 * blizzard hash 计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
size_t clib_hash_blizzard_from_string(char const *string, size_t seed);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_HASH_BLIZZARD_H
