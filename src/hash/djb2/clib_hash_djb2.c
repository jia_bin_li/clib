
#include "clib_hash_djb2.h"


size_t clib_hash_djb2(clib_uint8_t const *data, size_t size, size_t seed) {
    clib_check_if_true_return_value(data == NULL, 0);
    clib_check_if_true_return_value(size <= 0, 0);
    // 初始值
    size_t value = 5381;
    if (seed) {
        value = value * 33 + seed;
    }
    while (size--) value = (value * 33) + (*data++);
    return value;
}


size_t clib_hash_djb2_from_string(char const *string, size_t seed) {
    clib_check_if_true_return_value(string == NULL, 0);
    return clib_hash_djb2((clib_uint8_t const *) string, strlen(string) + 1, seed);
}
