
#ifndef CLIB_CLIB_HASH_DJB2_H
#define CLIB_CLIB_HASH_DJB2_H


#include "../../base/clib_base.h"


CLIB_CPLUS_SUPPORT_START

/**
 * djb2 hash计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
size_t clib_hash_djb2(clib_uint8_t const *data, size_t size, size_t seed);


/**
 * djb2 hash计算 字符串计算
 * @param string 字符串
 * @param seed 种子
 * @return
 */
size_t clib_hash_djb2_from_string(char const *string, size_t seed);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_HASH_DJB2_H
