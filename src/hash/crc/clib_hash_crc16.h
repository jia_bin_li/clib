#ifndef CLIB_CLIB_HASH_CRC16_H
#define CLIB_CLIB_HASH_CRC16_H



#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

/**
 * ANSI标准crc16
 * @param data 数据
 * @param size 数据长度
 * @param seed 种子
 * @return
 */
clib_uint16_t clib_hash_crc16_ansi(clib_uint8_t const *data, size_t size, clib_uint16_t seed);

/**
 * ANSI标准crc16 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint16_t clib_hash_crc16_ansi_from_string(char const *string, clib_uint16_t seed);

/**
 * CCITT标准crc16
 * @param data 数据
 * @param size 大小
 * @param seed 种子
 * @return
 */
clib_uint16_t clib_hash_crc16_ccitt(clib_uint8_t const *data, size_t size, clib_uint16_t seed);

/**
 * CCITT标准crc16 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint16_t clib_hash_crc16_ccitt_from_string(char const *string, clib_uint16_t seed);


CLIB_CPLUS_SUPPORT_END

#endif //CLIB_CLIB_HASH_CRC16_H
