
#ifndef CLIB_CLIB_HASH_CRC8_H
#define CLIB_CLIB_HASH_CRC8_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

/**
 * ANSI标准crc8
 * @param data 数据
 * @param size 大小
 * @param seed 种子
 * @return
 */
clib_uint8_t clib_hash_crc8_ansi(clib_uint8_t const *data, size_t size, clib_uint8_t seed);


/**
 * ANSI标准crc8 计算字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint8_t clib_hash_crc8_ansi_from_string(char const *string, clib_uint8_t seed);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_HASH_CRC8_H
