//
// Created by dpdk on 2022/6/22.
//

#ifndef CLIB_CLIB_HASH_CRC32_H
#define CLIB_CLIB_HASH_CRC32_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

/**
 * 大端模式计算CRC32
 * @param data 数据
 * @param size 数据长度
 * @param seed 种子
 * @return 计算后的CRC值
 */
clib_uint32_t clib_hash_crc32_be(clib_uint8_t const *data, size_t size, clib_uint32_t seed);

/**
 * 大端模式计算字符串的CRC32
 * @param string 字符串
 * @param seed 种子
 * @return 计算后的CRC值
 */
clib_uint32_t clib_hash_crc32_from_string_be(char const *string, clib_uint32_t seed);


/**
 * 小端模式计算CRC32
 * @param data 数据
 * @param size 数据长度
 * @param seed 种子
 * @return 计算后的CRC值
 */
clib_uint32_t clib_hash_crc32_le(clib_uint8_t const *data, size_t size, clib_uint32_t seed);

/**
 * 小端模式计算字符串的CRC32
 * @param string 字符串
 * @param seed 种子
 * @return 计算后的CRC值
 */
clib_uint32_t clib_hash_crc32_from_string_le(char const *string, clib_uint32_t seed);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_HASH_CRC32_H
