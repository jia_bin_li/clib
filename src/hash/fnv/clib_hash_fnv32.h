
#ifndef CLIB_CLIB_HASH_FNV32_H
#define CLIB_CLIB_HASH_FNV32_H

#include "../../base/clib_base.h"


CLIB_CPLUS_SUPPORT_START

/**
 * fnv32_1 hash 计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_fnv32_1(clib_uint8_t const *data, size_t size, clib_uint32_t seed);


/**
 * fnv32_1 hash 计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_fnv32_1_from_string(char const *string, clib_uint32_t seed);


/**
 * fnv32_1a hash 计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_fnv32_1a(clib_uint8_t const *data, size_t size, clib_uint32_t seed);


/**
 * fnv32_1a hash 计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_fnv32_1a_from_string(char const *string, clib_uint32_t seed);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_HASH_FNV32_H
