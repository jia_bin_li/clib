#ifndef CLIB_CLIB_HASH_MD5_H
#define CLIB_CLIB_HASH_MD5_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

#define CLIB_HASH_MD5_RESULT_LENGTH 16

struct clib_hash_md5 {
    //处理的 _bits_ 数量 mod 2^64
    clib_uint32_t i[2];
    //暂存缓冲区
    clib_uint32_t sp[4];
    //输入缓冲器
    clib_uint8_t ip[64];
    //生成后的实际数据
    clib_uint8_t data[CLIB_HASH_MD5_RESULT_LENGTH];
};

/**
 * 简单一次性计算md5值
 * @param data 数据
 * @param size 数据大小
 * @param out_data 计算后的数据
 * @return
 */
int clib_hash_md5_simple(clib_uint8_t const *in_data, size_t in_size, clib_uint8_t *out_data);

/**
 * MD5初始化
 * @param md5 md5结构体
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_hash_md5_init(struct clib_hash_md5 *md5);


/**
 * MD5 更新
 * @param md5 md5结构体
 * @param data 数据
 * @param size 数据大小
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_hash_md5_update(struct clib_hash_md5 *md5, clib_uint8_t const *data, size_t size);


/**
 * MD5 生成
 * @param md5 md5结构体
 * @param data 生成的数据 需要16个字节大小
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_hash_md5_final(struct clib_hash_md5 *md5, clib_uint8_t *data);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_HASH_MD5_H
