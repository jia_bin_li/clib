
#ifndef CLIB_CLIB_HASH_ADLER32_H
#define CLIB_CLIB_HASH_ADLER32_H

#include "../../base/clib_base.h"
CLIB_CPLUS_SUPPORT_START
/**
 * adler32 hash计算
 * @param data 数据
 * @param size 数据长度
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_adler32(clib_uint8_t const *data, size_t size, clib_uint32_t seed);


/**
 * adler32 hash计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_adler32_from_string(char const *string, clib_uint32_t seed);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_HASH_ADLER32_H
