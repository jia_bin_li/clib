
#include "clib_hash_ap.h"


size_t clib_hash_ap(clib_uint8_t const *data, size_t size, size_t seed) {
    clib_check_if_true_return_value(data == NULL, 0);
    clib_check_if_true_return_value(size == 0, 0);
    size_t v = 0xAAAAAAAA;
    if (seed) {
        v ^= seed;
    }
    size_t i = 0;
    clib_uint8_t const *p = data;
    for (i = 0; i < size; i++, p++) {
        v ^= (!(i & 1)) ? ((v << 7) ^ ((*p) * (v >> 3))) : (~(((v << 11) + (*p)) ^ (v >> 5)));
    }
    return v;
}

size_t clib_hash_ap_from_string(char const *string, size_t seed) {
    clib_check_if_true_return_value(string == NULL, 0);
    return clib_hash_ap((clib_uint8_t const *) string, strlen(string) + 1, seed);
}
