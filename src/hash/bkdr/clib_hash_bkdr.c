
#include "clib_hash_bkdr.h"

size_t clib_hash_bkdr(clib_uint8_t const *data, size_t size, size_t seed) {
    clib_check_if_true_return_value(data == NULL, 0);
    clib_check_if_true_return_value(size == 0, 0);
    size_t value = 0;
    if (seed) {
        value = value * 131313 + seed;
    }
    while (size--) {
        value = (value * 131313) + (*data++);
    }
    return value;
}

size_t clib_hash_bkdr_from_string(char const *string, size_t seed) {
    clib_check_if_true_return_value(string == NULL, 0);
    return clib_hash_bkdr((clib_uint8_t const *) string, strlen(string) + 1, seed);
}