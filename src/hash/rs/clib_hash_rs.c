
#include "clib_hash_rs.h"


size_t clib_hash_rs(clib_uint8_t const *data, size_t size, size_t seed) {
    clib_check_if_true_return_value(data == NULL, 0);
    clib_check_if_true_return_value(size == 0, 0);
    size_t value = seed;
    size_t b = 378551;
    size_t a = 63689;
    while (size--) {
        value = value * a + (*data++);
        a = a * b;
    }
    return value;
}

size_t clib_hash_rs_from_string(char const *string, size_t seed) {
    clib_check_if_true_return_value(string == NULL, 0);
    return clib_hash_rs((clib_uint8_t const *) string, strlen(string) + 1, seed);
}