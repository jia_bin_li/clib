#ifndef CLIB_CLIB_BASE_H
#define CLIB_CLIB_BASE_H

#include <stdbool.h>
#include <stdlib.h>
//C++调用支持
#include "cplus/clib_base_cplus.h"
//类型支持
#include "type/clib_base_type.h"
//锁支持
#include "lock/clib_base_lock.h"
//通用检查支持
#include "check/clib_base_check.h"
//CPU
#include "cpu/clib_base_cpu_info.h"
//字节对齐
#include "align/clib_base_align.h"

#endif //CLIB_CLIB_BASE_H
