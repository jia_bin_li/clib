#ifndef CLIB_CLIB_BASE_CPLUS_H
#define CLIB_CLIB_BASE_CPLUS_H

#ifdef __cplusplus
#   define CLIB_CPLUS_SUPPORT_START                extern "C" {
#   define CLIB_CPLUS_SUPPORT_END                  }
#else
#   define CLIB_CPLUS_SUPPORT_START
#   define CLIB_CPLUS_SUPPORT_END
#endif

#endif //CLIB_CLIB_BASE_CPLUS_H
