//
// Created by Administrator on 2022-06-13.
//

#ifndef CLIB_CLIB_BASE_CHECK_H
#define CLIB_CLIB_BASE_CHECK_H


//一些通用的检查语句

#define clib_check_if_false_return(x)                              do { if (!(x)) return ; } while (0)
#define clib_check_if_false_return_value(x, v)                     do { if (!(x)) return (v); } while (0)
#define clib_check_if_false_goto(x, b)                             do { if (!(x)) goto b; } while (0)
#define clib_check_if_false_break(x)                               { if (!(x)) break ; }
#define clib_check_if_false_continue(x)                            { if (!(x)) continue ; }

#define clib_check_if_true_return(x)                              do { if ((x)) return ; } while (0)
#define clib_check_if_true_return_value(x, v)                     do { if ((x)) return (v); } while (0)
#define clib_check_if_true_exec(x)                           if ((x))
#define clib_check_if_true_goto(x, b)                             do { if ((x)) goto b; } while (0)
#define clib_check_if_true_break(x)                               { if ((x)) break ; }
#define clib_check_if_true_continue(x)                            { if ((x)) continue ; }

#endif //CLIB_CLIB_BASE_CHECK_H
