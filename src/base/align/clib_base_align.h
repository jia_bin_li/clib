//
// Created by Administrator on 2022-06-16.
//

#ifndef CLIB_CLIB_BASE_ALIGN_H
#define CLIB_CLIB_BASE_ALIGN_H

#include "../type/clib_base_type.h"
#include "../bits/clib_base_bits.h"

/// 2字节对齐
#define clib_align2(x)                    (((x) + 1) >> 1 << 1)

/// 4字节对齐
#define clib_align4(x)                    (((x) + 3) >> 2 << 2)

/// 8字节对齐
#define clib_align8(x)                    (((x) + 7) >> 3 << 3)

/// u_size_t 对齐
#define clib_align(x, b)                  (((size_t)(x) + ((size_t)(b) - 1)) & ~((size_t)(b) - 1))

/// 对齐 u32
#define clib_align_u32(x, b)              (((size_t)(x) + ((size_t)(b) - 1)) & ~((size_t)(b) - 1))

/// 对齐 u64
#define clib_align_u64(x, b)              (((size_t)(x) + ((size_t)(b) - 1)) & ~((size_t)(b) - 1))


// 是否是2的次方数
#define clib_align_is_pow2(x)                    (!((x) & ((x) - 1)) && (x))

// 对其2的次方数
#define clib_align_pow2(x)                (((x) > 1)? (clib_align_is_pow2(x)? (x) : ((size_t)1 << (32 - clib_bits_count_leading_bit0_u32_be((clib_uint32_t)(x))))) : 1)


#endif //CLIB_CLIB_BASE_ALIGN_H
