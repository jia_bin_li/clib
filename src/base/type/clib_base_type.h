//
// Created by Administrator on 2022-06-13.
//

#ifndef CLIB_CLIB_BASE_TYPE_H
#define CLIB_CLIB_BASE_TYPE_H

#include <string.h>

//int 最大值
#define INT8_MIN (-128)
#define INT16_MIN (-32768)
#define INT32_MIN (-2147483647 - 1)
#define INT64_MIN  (-9223372036854775807LL - 1)

#define INT8_MAX 127
#define INT16_MAX 32767
#define INT32_MAX 2147483647
#define INT64_MAX 9223372036854775807LL

#define UINT8_MAX 255
#define UINT16_MAX 65535
#define UINT32_MAX 0xffffffffU  /* 4294967295U */
#define UINT64_MAX 0xffffffffffffffffULL /* 18446744073709551615ULL */

//int 类型
typedef signed char clib_int8_t;
typedef unsigned char clib_uint8_t;
typedef short clib_int16_t;
typedef unsigned short clib_uint16_t;
typedef int clib_int32_t;
typedef unsigned clib_uint32_t;
typedef long long clib_int64_t;
typedef unsigned long clib_uint64_t;

//返回值定义
#define CLIB_RES_OK 0
#define CLIB_RES_PARAM_ERROR (-1)
#define CLIB_RES_MEMORY_ERROR (-2)
#define CLIB_RES_OUT_OF_BOUNDS_ERROR (-3)
#define CLIB_RES_OTHER_ERROR (-4)


//强转
#define CLIB_CAST_TO_UINT8(x)                      ((clib_uint8_t)(x))
#define CLIB_CAST_TO_UINT8_POINT(x)                      ((clib_uint8_t*)(x))
#define CLIB_CAST_TO_UINT16(x)                     ((clib_uint16_t)(x))
#define CLIB_CAST_TO_UINT16_POINT(x)                     ((clib_uint16_t*)(x))
#define CLIB_CAST_TO_UINT32(x)                     ((clib_uint32_t)(x))
#define CLIB_CAST_TO_UINT32_POINT(x)                     ((clib_uint32_t*)(x))
#define CLIB_CAST_TO_UINT64(x)                     ((clib_uint64_t)(x))
#define CLIB_CAST_TO_UINT64_POINT(x)                     ((clib_uint64_t*)(x))
#define CLIB_CAST_TO_INT8(x)                     ((clib_int8_t)(x))
#define CLIB_CAST_TO_INT8_POINT(x)                     ((clib_int8_t*)(x))
#define CLIB_CAST_TO_INT16(x)                     ((clib_int16_t)(x))
#define CLIB_CAST_TO_INT16_POINT(x)                     ((clib_int16_t*)(x))
#define CLIB_CAST_TO_INT32(x)                     ((clib_int32_t)(x))
#define CLIB_CAST_TO_INT32_POINT(x)                     ((clib_int32_t*)(x))
#define CLIB_CAST_TO_INT64(x)                     ((clib_int64_t)(x))
#define CLIB_CAST_TO_INT64_POINT(x)                     ((clib_int64_t*)(x))
#define CLIB_CAST_TO_SIZE(x)                     ((size_t)(x))
#define CLIB_CAST_TO_SIZE_POINT(x)                     ((size_t*)(x))
#endif //CLIB_CLIB_BASE_TYPE_H
