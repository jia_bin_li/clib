#ifndef CLIB_CLIB_CONTAINER_FIFO_QUEUE_H
#define CLIB_CLIB_CONTAINER_FIFO_QUEUE_H

#include "../../base/clib_base.h"


CLIB_CPLUS_SUPPORT_START

/**
 * 前进先出队列对象
 */
struct clib_container_fifo_queue;

/**
 * 先进先出队列配置
 */
struct clib_container_fifo_queue_conf {
    /**
    * 内存分配函数
    * @param size 需要分配的大小
    * @return
    */
    void *(*mem_malloc_func)(size_t size);

    /**
     * 内存释放函数
     * @param block 块指针
     */
    void (*mem_free_func)(void *block);

    /**
     * 内存拷贝函数
     * @param target 目标
     * @param src 源
     * @param size 大小
     * @return
     */
    void *(*mem_copy_func)(void *target, const void *src, size_t size);
};

/**
 * 创建一个先进先出队列
 * @param conf 配置信息
 * @param queue 存储创建好的队列对象
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_container_fifo_queue_create_with_conf(
        struct clib_container_fifo_queue_conf *conf,
        struct clib_container_fifo_queue **queue);

/**
 * 销毁一个先进先出队列
 * @param queue 队列对象
 */
void clib_container_fifo_queue_destroy(
        struct clib_container_fifo_queue *queue);


/**
 * 查看队列首部元素
 * @param queue
 * @param out
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_container_fifo_queue_peek(
        struct clib_container_fifo_queue *queue,
        void **out);

/**
 * 出队
 * @param queue 队列对象
 * @param out 出队的队列对象
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_container_fifo_queue_dequeue(
        struct clib_container_fifo_queue *queue,
        void **out);

/**
 * 入队
 * @param queue 队列对象
 * @param element 入队的元素
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_container_fifo_queue_enqueue(
        struct clib_container_fifo_queue *queue,
        void *element);

/**
 * 获取当前元素个数
 * @param queue 队列对象
 * @return 元素个数
 */
size_t clib_container_fifo_queue_get_current_size(
        struct clib_container_fifo_queue *queue);

CLIB_CPLUS_SUPPORT_END

#endif //CLIB_CLIB_CONTAINER_FIFO_QUEUE_H
