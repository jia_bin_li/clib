
#ifndef CLIB_CLIB_RANDOM_CLOCK_H
#define CLIB_CLIB_RANDOM_CLOCK_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

/**
 * 使用系统时间作为随机种子并获取随机数
 * 每次调用函数都会使用系统纳秒值作为种子重新获取随机值
 * @return
 */
clib_int64_t clib_random_clock();

/**
 * 使用系统时间作为随机种子,按照范围获取随机值 [begin, end)
 * @param begin 开始值
 * @param end 结束值
 * @return
 */
clib_int64_t clib_random_clock_range(clib_int64_t begin, clib_int64_t end);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_RANDOM_CLOCK_H
