#ifndef CLIB_CLIB_RANDOM_UUID_H
#define CLIB_CLIB_RANDOM_UUID_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

/**
 * UUID长度
 */
#define CLIB_RANDOM_UUID_LENGTH 16
/**
 * UUID字符串长度
 */
#define CLIB_RANDOM_UUID_STRING_LENGTH 37


/**
 * 获取UUID 字节数组
 * @param uuid UUID返回字符串
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_random_uuid(
        clib_uint8_t uuid[CLIB_RANDOM_UUID_LENGTH]);


/**
 * 获取UUID 字符串
 * @param uuid UUID返回字符串
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_random_uuid_to_string(
        char uuid[CLIB_RANDOM_UUID_STRING_LENGTH]);


CLIB_CPLUS_SUPPORT_END

#endif //CLIB_CLIB_RANDOM_UUID_H
