#include "clib_time.h"

CLIB_CPLUS_SUPPORT_START

#include <sys/time.h>

clib_uint32_t clib_time_get_second_timestamp() {
    struct timespec timestamp;
    clock_gettime(CLOCK_REALTIME, &timestamp);
    return timestamp.tv_sec;
}

clib_uint64_t clib_time_get_nanosecond_timestamp() {
    struct timespec timestamp;
    clock_gettime(CLOCK_REALTIME, &timestamp);
    return timestamp.tv_sec * 1000000000 + timestamp.tv_nsec;
}

clib_uint64_t clib_time_get_microsecond_timestamp() {
    return clib_time_get_nanosecond_timestamp() / 1000;
}

clib_uint64_t clib_time_get_millisecond_timestamp() {
    return clib_time_get_nanosecond_timestamp() / 1000000;
}


CLIB_CPLUS_SUPPORT_END