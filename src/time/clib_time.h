#ifndef CLIB_CLIB_TIME_H
#define CLIB_CLIB_TIME_H

#include "../base/clib_base.h"
CLIB_CPLUS_SUPPORT_START
/**
 * 获取当前秒数时间戳
 * @return
 */
clib_uint32_t clib_time_get_second_timestamp();

/**
 * 获取当前毫秒时间戳
 * @return
 */
clib_uint64_t clib_time_get_millisecond_timestamp();


/**
 * 获取当前的微秒时间戳
 * @return
 */
clib_uint64_t clib_time_get_microsecond_timestamp();

/**
 * 获取当前纳秒时间戳
 * @return
 */
clib_uint64_t clib_time_get_nanosecond_timestamp();

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_TIME_H
