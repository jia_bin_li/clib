#ifndef CLIB_CLIB_MEMORY_ALLOCATOR_H
#define CLIB_CLIB_MEMORY_ALLOCATOR_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

enum clib_allocator_type {
    CLIB_ALLOCATOR_TYPE_DEFAULT,
    CLIB_ALLOCATOR_TYPE_STANDARD,
    CLIB_ALLOCATOR_TYPE_NATIVE,
    CLIB_ALLOCATOR_TYPE_NUM
};


enum clib_allocator_flags {
    CLIB_ALLOCATOR_FLAG_USE_LOCK = 0x01,
    CLIB_ALLOCATOR_FLAG_NO_USE_LOCK = 0x02,
};


struct clib_memory_allocator {
    //内存分配器类型
    clib_uint16_t type;
    //内存分类器标记
    clib_uint8_t flag;
    //内存分类器锁
    spinlock_t lock;

    //malloc 函数声明
    void *(*malloc)(clib_uint64_t size);

    //re_alloc 函数声明
    void *(*re_alloc)(void *data, clib_uint64_t size);

    //free 函数声明
    void (*free)(void *data);

    //clear 函数声明
    void (*clear)();

    //exit 函数声明
    void (*exit)();

    //memcopy 函数声明
    void (*mem_copy)(void *target, void *src, size_t length);
};

/**
 * 初始化内存模块
 * @return 0成功
 */
int clib_memory_allocator_init(enum clib_allocator_type type);

/**
 * 分配内存空间 使用默认分配器
 * @param size
 * @return
 */
void *clib_memory_allocator_malloc_default(clib_uint64_t size);

/**
 * 重新分配内存
 * @param data
 * @param size
 * @return
 */
void *clib_memory_allocator_re_alloc_default(void *data, clib_uint64_t size);


/**
 * 释放内存空间 使用默认分配器
 * @param size
 * @return
 */
void clib_memory_allocator_free_default(void *data);


/**
 * 内存拷贝
 * @param target 目标内存
 * @param src  要拷贝的内存
 * @param length  长度
 */
void clib_memory_allocator_mem_copy_default(void *target, void *src, size_t length);

/**
 * 分配内存空间  使用指定分配器
 * @param size
 * @param type
 * @return
 */
void *clib_memory_allocator_malloc(clib_uint64_t size, enum clib_allocator_type type);


/**
 * 重新分配内存
 * @param data
 * @param size
 * @param type
 * @return
 */
void *clib_memory_allocator_re_alloc(void *data, clib_uint64_t size, enum clib_allocator_type type);

/**
 * 释放内存空间  使用指定分配器
 * @param size
 * @param type
 * @return
 */
void clib_memory_allocator_free(void *data, enum clib_allocator_type type);


/**
 * 内存拷贝 使用指定分配器
 * @param target
 * @param src
 * @param length
 * @param type
 */
void clib_memory_allocator_mem_copy(void *target, void *src, size_t length, enum clib_allocator_type type);

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_MEMORY_ALLOCATOR_H
