#ifndef CLIB_CLIB_MEMORY_ALLOCATOR_STANDARD_H
#define CLIB_CLIB_MEMORY_ALLOCATOR_STANDARD_H

#include "clib_memory_allocator.h"
CLIB_CPLUS_SUPPORT_START
/**
 * 初始化标准内存分配器
 * @return 0成功
 */
int clib_memory_allocator_standard_init();

/**
 * 获取标准的内存分配器
 * @return 成功返回分配器指针，失败返回NULL
 */
struct clib_memory_allocator *clib_memory_allocator_standard_get();

CLIB_CPLUS_SUPPORT_END
#endif //CLIB_CLIB_MEMORY_ALLOCATOR_STANDARD_H
