#ifndef CLIB_CLIB_MEMORY_STATIC_FIXED_POOL_H
#define CLIB_CLIB_MEMORY_STATIC_FIXED_POOL_H

#include <stdbool.h>
#include "../clib_memory.h"

CLIB_CPLUS_SUPPORT_START

/**
 * 静态固定元素大小内存池结构体
 */
struct clib_memory_static_fixed_pool;


/**
 * 使用外部内存块创建静态固定元素大小内存池
 * @param pool_data 外部数据内存指针
 * @param pool_size 外部数据内存大小
 * @param item_size 每个元素的内存大小
 * @return
 * 成功 返回静态固定元素大小内存池
 * 失败 返回NULL
 */
struct clib_memory_static_fixed_pool *
clib_memory_static_fixed_pool_create(clib_uint8_t *pool_data, size_t pool_size, size_t item_size);

/**
 * 自动申请内存并创建静态固定元素大小内存池
 * @param item_size 每个元素的大小
 * @param item_count 元素的个数
 * @return
 * 成功 返回静态固定元素大小内存池
 * 失败 返回NULL
 */
struct clib_memory_static_fixed_pool *
clib_memory_static_fixed_pool_auto_create(size_t item_size, size_t item_count);


/**
 * 分配内存
 * @param pool 内存池
 * @return
 */
void *clib_memory_static_fixed_pool_malloc(struct clib_memory_static_fixed_pool *pool);


/**
 * 释放内存
 * @param pool 内存池
 * @param data 要释放的数据
 * @return
 */
bool clib_memory_static_fixed_pool_free(struct clib_memory_static_fixed_pool *pool, void *data);


/**
 * 清空内存池
 * @param pool 内存池
 */
void
clib_memory_static_fixed_pool_clear(struct clib_memory_static_fixed_pool *pool);


/**
 * 关闭内存池
 * 如果是自动分配的内存，则将自动释放
 * 如果是外部提供的内存，则只是清空内存，不进行释放
 * @param pool 内存池
 */
void clib_memory_static_fixed_pool_exit(struct clib_memory_static_fixed_pool *pool);

/**
 * 获取内存池中能容纳的最大元素个数
 * @param pool 内存池
 * @return
 * 成功 返回最大元素个数
 * 失败 返回0
 */
size_t
clib_memory_static_fixed_pool_get_max_item_count(struct clib_memory_static_fixed_pool *pool);


/**
 * 获取内存池中每个元素的大小
 * @param pool 内存池
 * @return
 * 成功 返回元素大小
 * 失败 返回0
 */
size_t
clib_memory_static_fixed_pool_get_item_size(struct clib_memory_static_fixed_pool *pool);

/**
 * 获取内存池中当前元素个数
 * @param pool 内存池
 * @return
 * 成功 返回当前元素个数
 */
size_t
clib_memory_static_fixed_pool_get_current_item_count(struct clib_memory_static_fixed_pool *pool);


CLIB_CPLUS_SUPPORT_END

#endif //CLIB_CLIB_MEMORY_STATIC_FIXED_POOL_H
