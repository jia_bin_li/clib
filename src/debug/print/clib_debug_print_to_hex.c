#include <stdio.h>
#include "clib_debug_print_to_hex.h"

#define clib_print_hex_isgraph(x)               ((x) > 0x1f && (x) < 0x7f)


void clib_debug_print_to_hex(clib_uint8_t const *data, size_t size) {
    clib_check_if_true_return(data == NULL);
    clib_check_if_true_return(size == 0);
    clib_check_if_true_return(size > CLIB_DEBUG_PRINT_TO_HEX_MAX_SIZE);
    static const char *hex = "0123456789abcdef";
    int q, w;
    char hex_data[CLIB_DEBUG_PRINT_TO_HEX_MAX_SIZE];
    for (q = 0, w = 0; q < size; q++) {
        if (w > CLIB_DEBUG_PRINT_TO_HEX_MAX_SIZE) {
            break;
        }
        hex_data[w++] = hex[(data[q] >> 4) & 0x0F];
        hex_data[w++] = hex[data[q] & 0x0F];
    }
    hex_data[w] = '\0';

    for (int i = 1; i <= w; ++i) {
        if (i % 2 == 0) {
            printf("%c ", hex_data[i - 1]);
        } else {
            printf("%c", hex_data[i - 1]);
        }
        if (i % 8 == 0) {
            printf("  ");
        }
        if (i % 32 == 0) {
            printf("\n");
        }
    }
}