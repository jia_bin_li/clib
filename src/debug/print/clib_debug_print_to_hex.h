#ifndef CLIB_CLIB_DEBUG_PRINT_TO_HEX_H
#define CLIB_CLIB_DEBUG_PRINT_TO_HEX_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

//最大打印长度
#define CLIB_DEBUG_PRINT_TO_HEX_MAX_SIZE 8192

/**
 * 将二进制数据打印为16进制
 * @param data 要打印的数据
 * @param size 要打印的长度
 */
void clib_debug_print_to_hex(clib_uint8_t const *data, size_t size);

CLIB_CPLUS_SUPPORT_END

#endif //CLIB_CLIB_DEBUG_PRINT_TO_HEX_H
