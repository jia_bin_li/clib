
#ifndef CLIB_CLIB_CODED_BASE32_H
#define CLIB_CLIB_CODED_BASE32_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

/**
 * 计算输出的base32编码最少需要的存储空间
 */
#define CLIB_CODED_BASE32_OUTPUT_MIN(in)  clib_align8(((((in) * 8) / 5) + (((in) % 5) != 0) + 1))

/**
 * 对指定数据进行base32编码
 * @param in_data 输入数据
 * @param in_size 输入数据大小
 * @param out_data  输出数据
 * @param out_size 输出数据大小
 * @return 实际输出大小
 */
size_t clib_coded_base32_encode(clib_uint8_t const *in_data, size_t in_size, char *out_data, size_t out_size);



/**
 * 对指定base32编码字符串进行解码
 * @param in_data 输入数据
 * @param in_size 输入数据的大小
 * @param out_data 输出数据
 * @param out_size 输出数据的大小
 * @return
 */
size_t clib_coded_base32_decode(char const *in_data, size_t in_size, clib_uint8_t *out_data, size_t out_size);



CLIB_CPLUS_SUPPORT_END

#endif //CLIB_CLIB_CODED_BASE32_H
