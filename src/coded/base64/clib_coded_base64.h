#ifndef CLIB_CLIB_CODED_BASE64_H
#define CLIB_CLIB_CODED_BASE64_H

#include "../../base/clib_base.h"

CLIB_CPLUS_SUPPORT_START

//BASE64输出最小空间值
#define CLIB_CODED_BASE64_OUTPUT_MIN(in)  (((in) + 2) / 3 * 4 + 1)

/**
 * base64编码
 * @param in_data 输入数据
 * @param in_size 输入数据大小
 * @param out_data 输出数据
 * @param out_size 输出数据大小
 * @return 输出数据的真实大小
 */
size_t clib_coded_base64_encode(
        clib_uint8_t const *in_data,
        size_t in_size,
        char *out_data,
        size_t out_size);


/**
 * base64解码
 * @param in_data 输入数据
 * @param in_size 输入数据大小
 * @param out_data 输出数据
 * @param out_size 输出数据大小
 * @return 输出数据的真实大小
 */
size_t clib_coded_base64_decode(
        char const *in_data,
        size_t in_size,
        clib_uint8_t *out_data,
        size_t out_size);

CLIB_CPLUS_SUPPORT_END

#endif //CLIB_CLIB_CODED_BASE64_H
