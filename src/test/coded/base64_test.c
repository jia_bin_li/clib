
#include <stdio.h>
#include "../../coded/base64/clib_coded_base64.h"


static void test() {
    char *data = "abcdefg";
    int result_length = CLIB_CODED_BASE64_OUTPUT_MIN(strlen(data));
    printf("%d\n",result_length);
    char result_data[result_length];
    size_t real_size = clib_coded_base64_encode((clib_uint8_t *) data, strlen(data), result_data,
                                                result_length);
    printf("%zu %s\n", real_size, result_data);

    char decode_result[strlen(data)];
    size_t decode_size = clib_coded_base64_decode(result_data,result_length,(clib_uint8_t*)decode_result, strlen(data));
    printf("%zu %s\n", decode_size, decode_result);
}


static void test_base64_main() {
    test();
}