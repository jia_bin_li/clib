#include <stdio.h>
#include "../../time/clib_time.h"


static void time_test() {
    for (int i = 0; i < 10; ++i) {
        //获取秒值
        clib_uint32_t second = clib_time_get_second_timestamp();
        printf("second %u\n", second);
        //获取毫秒
        clib_uint64_t millisecond = clib_time_get_millisecond_timestamp();
        printf("millisecond %lu\n", millisecond);
        //获取微秒
        clib_uint64_t microsecond = clib_time_get_microsecond_timestamp();
        printf("microsecond %lu\n", microsecond);
        //纳秒
        clib_uint64_t nanosecond = clib_time_get_nanosecond_timestamp();
        printf("nanosecond %lu\n", nanosecond);
    }
}


static void time_test_main(){
    time_test();
}