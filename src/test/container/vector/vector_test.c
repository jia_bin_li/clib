#include <malloc.h>
#include <stdlib.h>
#include "../../../container/vector/clib_container_vector.h"

static struct clib_container_vector *vector;


static void show() {
    size_t current_size = clib_container_vector_get_current_size(vector);
    printf("current %zu\n", current_size);
    for (int i = 0; i < current_size; ++i) {
        void *data;
        int code = clib_container_vector_get_with_index(vector, i, &data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_get_with_index error\n");
            exit(0);
        }
        printf("%d ", *(clib_uint32_t *) data);
    }
    printf("\n");
}

static void test_create() {
    struct clib_container_vector_conf conf = {
            .mem_copy_func =memcpy,
            .mem_free_func = free,
            .mem_malloc_func = malloc
    };
    int code = clib_container_vector_create_with_conf(&conf, &vector);
    if (code != CLIB_RES_OK) {
        printf("clib_container_vector_create_with_conf error\n");
        exit(0);
    }
}


static void test_add() {
    for (int i = 0; i < 1000; ++i) {
        clib_uint32_t *data = malloc(sizeof(clib_uint32_t));
        *data = i;
        int code = clib_container_vector_add(vector, data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_add error\n");
            exit(0);
        }
    }
    show();

    for (int i = 0; i < 100; ++i) {
        clib_uint32_t *data = malloc(sizeof(clib_uint32_t));
        *data = i;
        int code = clib_container_vector_add_with_index(vector, data, 1);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_add error\n");
            exit(0);
        }
    }
    show();
}

static void test_remove() {
    for (int i = 0; i < 1000; ++i) {
        clib_uint32_t *data = malloc(sizeof(clib_uint32_t));
        *data = i;
        int code = clib_container_vector_add(vector, data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_add error\n");
            exit(0);
        }
    }
    show();

    for (int i = 0; i < 100; ++i) {
        void *remove_data;
        int code = clib_container_vector_remove_with_index(vector, 0, &remove_data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_remove_with_index error\n");
            exit(0);
        }
        free(remove_data);
    }
    show();

    for (int i = 0; i < 100; ++i) {
        void *remove_data;
        int code = clib_container_vector_remove_last(vector, &remove_data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_remove_last error\n");
            exit(0);
        }
        free(remove_data);
    }
    show();

    for (int i = 0; i < 100; ++i) {
        void *remove_data;
        int code = clib_container_vector_get_with_index(vector, 0, &remove_data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_get_with_index error\n");
            exit(0);
        }
        code = clib_container_vector_remove(vector, remove_data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_remove error\n");
            exit(0);
        }
        free(remove_data);
    }
    show();
    clib_container_vector_destroy(vector);
}


static void test_clear() {
    for (int i = 0; i < 1000; ++i) {
        clib_uint32_t *data = malloc(sizeof(clib_uint32_t));
        *data = i;
        int code = clib_container_vector_add(vector, data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_add error\n");
            exit(0);
        }
    }
    show();

    for (int i = 0; i < 1000; ++i) {
        void *remove_data;
        int code = clib_container_vector_remove_with_index(vector, 0, &remove_data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_vector_remove_with_index error\n");
            exit(0);
        }
        free(remove_data);
    }
    show();
}

static void test_vector_main() {
    test_create();
//    test_add();
//    test_remove();
//    test_clear();
}