#include "../../../container/queue/clib_container_fifo_queue.h"
#include "malloc.h"

static void test_fifo_queue() {

    struct clib_container_fifo_queue_conf conf = {
            .mem_free_func = free,
            .mem_malloc_func = malloc,
            .mem_copy_func = memcpy,
    };
    struct clib_container_fifo_queue *queue;
    int code = clib_container_fifo_queue_create_with_conf(&conf, &queue);
    if (code != CLIB_RES_OK) {
        printf("clib_container_fifo_queue_create_with_conf error\n");
        return;
    }
//    int * peek_data;
//    code = clib_container_fifo_queue_dequeue(queue, (void **) &peek_data);
//    if (code != CLIB_RES_OK){
//        printf("clib_container_fifo_queue_peek error\n");
//        return;
//    }

    for (int i = 0; i < 10000; ++i) {
        int *data = malloc(sizeof(int));
        *data = i;
        code = clib_container_fifo_queue_enqueue(queue, data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_fifo_queue_enqueue error\n");
            return;
        }
    }

    for (int i = 0; i < 5000; ++i) {
        int *data;
        code = clib_container_fifo_queue_dequeue(queue, (void **) &data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_fifo_queue_dequeue error\n");
            return;
        }
        printf("dequeue %d\n", *data);
        free(data);
    }

    for (int i = 0; i < 5000; ++i) {
        int *data = malloc(sizeof(int));
        *data = i;
        code = clib_container_fifo_queue_enqueue(queue, data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_fifo_queue_enqueue error\n");
            return;
        }
    }

    for (int i = 0; i < 10000; ++i) {
        int *data;
        code = clib_container_fifo_queue_dequeue(queue, (void **) &data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_fifo_queue_dequeue error\n");
            return;
        }
        printf("dequeue %d\n", *data);
        free(data);
    }

    clib_container_fifo_queue_destroy(queue);

}


static void test_queue_main() {
    test_fifo_queue();
}