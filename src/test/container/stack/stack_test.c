#include "../../../container/stack/clib_container_stack.h"
#include <malloc.h>

static void test_stack() {
    struct clib_container_stack_conf conf = {
            .mem_free_func = free,
            .mem_malloc_func = malloc,
            .mem_copy_func = memcpy
    };

    struct clib_container_stack *stack;
    int code = clib_container_stack_create_with_conf(&conf, &stack);
    if (code != CLIB_RES_OK) {
        printf("clib_container_stack_create_with_conf error\n");
        return;
    }
//    int * pop_data;
//    code = clib_container_stack_peek(stack, (void **) &pop_data);
//    if (code != CLIB_RES_OK){
//        printf("clib_container_stack_peek error %d\n",code);
//        return;
//    }

    for (int i = 0; i < 1000; ++i) {
        int *data = malloc(sizeof(int));
        *data = i;
        code = clib_container_stack_push(stack, data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_stack_push error %d\n", code);
            return;
        }
    }
    for (int i = 0; i < 1001; ++i) {
        int *data;
        code = clib_container_stack_pop(stack, (void **) &data);
        if (code != CLIB_RES_OK) {
            printf("clib_container_stack_pop error %d\n", code);
            return;
        }
        printf("pop %d\n", *data);
        free(data);
    }
}


static void test_stack_main() {
    test_stack();
}