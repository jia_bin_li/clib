
#include "../../../hash/crc/clib_hash_crc32.h"
#include "../../../container/hash_set/clib_container_hash_set.h"
#include <malloc.h>

static struct clib_container_hash_set *set;

static void show() {

    size_t current_size = clib_container_hash_set_get_current_size(set);
    printf("当前大小 %zu\n", current_size);

    void *values[current_size];

    size_t size = clib_container_hash_set_get_values(set, values);
    if (size != current_size) {
        //异常
        printf("clib_container_hash_set_get_values error\n");
        return;
    }
    printf("values: \n");
    for (int i = 0; i < size; ++i) {
        printf("%u ", *(clib_uint32_t *) values[i]);
    }
    printf("\n");
}

static void test_create() {
    struct clib_container_hash_set_conf conf = {
            .hash_func = (size_t (*)(const void *, size_t, clib_uint32_t)) clib_hash_crc32_le,
            .load_factor = 0.5f,
            .key_length = sizeof(clib_uint32_t),
            .key_compare_func = memcmp,
            .initial_capacity = 10,
            .hash_seed = 0,
            .mem_malloc_func = malloc,
            .mem_free_func = free,
            .mem_copy_func = memcpy
    };
    int code = clib_container_hash_set_create_with_conf(&conf, &set);
    if (code != CLIB_RES_OK) {
        return;
    }
    printf("创建成功\n");
    show();
}


static void test_put() {
    while (true){
        for (int i = 0; i < 10; ++i) {
            clib_uint32_t *value = malloc(sizeof(clib_uint32_t));
            *value = i;
            void *pre_data;
            int code = clib_container_hash_set_add(set, value, &pre_data);
            if (code != CLIB_RES_OK) {
                printf("clib_container_hash_set_add error\n");
                return;
            }
            if (pre_data != NULL) {
                free(pre_data);
            }
        }
        show();
        for (int i = 0; i < 10; ++i) {
            clib_uint32_t *value = malloc(sizeof(clib_uint32_t));
            *value = i;
            void *pre_data;
            int code = clib_container_hash_set_add(set, value, &pre_data);
            if (code != CLIB_RES_OK) {
                printf("clib_container_hash_set_add error\n");
                return;
            }
            if (pre_data != NULL) {
                free(pre_data);
            }
        }
        show();
//        for (int i = 0; i < 5; ++i) {
//            int remove_key = i;
//            void *pre_data;
//            int code = clib_container_hash_set_remove(set, &remove_key, &pre_data);
//            if (code != CLIB_RES_OK) {
//                printf("clib_container_hash_set_remove error\n");
//                return;
//            }
//            if (pre_data != NULL) {
//                free(pre_data);
//            }
//        }
//        show();
    }
}


static void test_hash_set() {

    test_create();
    test_put();
}