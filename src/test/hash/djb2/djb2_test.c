#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/djb2/clib_hash_djb2.h"


static void test_clib_hash_djb2() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    size_t result = clib_hash_djb2(data,4,0);
    printf("result %zu\n", result);
}


static void test_clib_hash_djb2_main(){
    test_clib_hash_djb2();
}