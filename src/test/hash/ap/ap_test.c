#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/ap/clib_hash_ap.h"


static void test_clib_hash_ap() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    size_t result = clib_hash_ap(data,4,0);
    printf("result %zu\n", result);
}


static void test_clib_hash_ap_main(){
    test_clib_hash_ap();
}

