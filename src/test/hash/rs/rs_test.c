#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/rs/clib_hash_rs.h"

static void test_clib_hash_rs() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    size_t result = clib_hash_rs(data, 4, 0);
    printf("result %zu\n", result);
}

static void test_clib_hash_rs_from_string() {
    size_t result = clib_hash_rs_from_string("hello", 0);
    printf("result %zu\n", result);
}


static void test_clib_hash_rs_main() {
    test_clib_hash_rs();
    test_clib_hash_rs_from_string();
}



