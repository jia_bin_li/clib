#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/murmur/clib_hash_murmur.h"

static void test_clib_hash_murmur() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    size_t result = clib_hash_murmur(data, 4, 0);
    printf("result %zu\n", result);
}

static void test_clib_hash_murmur_from_string() {
    size_t result = clib_hash_murmur_from_string("hello", 0);
    printf("result %zu\n", result);
}


static void test_clib_hash_murmur_main() {
    test_clib_hash_murmur();
    test_clib_hash_murmur_from_string();
}



