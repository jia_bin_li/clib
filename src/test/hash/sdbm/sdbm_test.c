#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/sdbm/clib_hash_sdbm.h"

static void test_clib_hash_sdbm() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    size_t result = clib_hash_sdbm(data, 4, 0);
    printf("result %zu\n", result);
}

static void test_clib_hash_sdbm_from_string() {
    size_t result = clib_hash_sdbm_from_string("hello", 0);
    printf("result %zu\n", result);
}


static void test_clib_hash_sdbm_main() {
    test_clib_hash_sdbm();
    test_clib_hash_sdbm_from_string();
}




