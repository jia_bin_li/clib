#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/md5/clib_hash_md5.h"

static void test_clib_hash_md5() {
    char *data = "123456";
    struct clib_hash_md5 md5;
    if (CLIB_RES_OK != clib_hash_md5_init(&md5)) {
        return;
    }
    if (CLIB_RES_OK != clib_hash_md5_update(&md5, CLIB_CAST_TO_UINT8_POINT(data), strlen(data))) {
        return;
    }
    clib_uint8_t result_md5[CLIB_HASH_MD5_RESULT_LENGTH];
    if (CLIB_RES_OK != clib_hash_md5_final(&md5, result_md5)) {
        return;
    }

    for (int i = 0; i < CLIB_HASH_MD5_RESULT_LENGTH; ++i) {
        printf("%02x", result_md5[i]);
    }
    printf("\n");

    clib_uint8_t result_md52[CLIB_HASH_MD5_RESULT_LENGTH];
    clib_hash_md5_simple(CLIB_CAST_TO_UINT8_POINT(data), strlen(data), result_md52);

    for (int i = 0; i < CLIB_HASH_MD5_RESULT_LENGTH; ++i) {
        printf("%02x", result_md52[i]);
    }
    printf("\n");
}

static void test_clib_hash_md5_main() {
    test_clib_hash_md5();
}