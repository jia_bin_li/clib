#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/fnv/clib_hash_fnv32.h"
#include "../../../hash/fnv/clib_hash_fnv64.h"

static void test_clib_hash_fnv() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    size_t result = clib_hash_fnv32_1(data, 4, 0);
    printf("result1 %zu\n", result);

    result = clib_hash_fnv32_1a(data, 4, 0);
    printf("result2 %zu\n", result);

    result = clib_hash_fnv64_1(data, 4, 0);
    printf("result3 %zu\n", result);

    result = clib_hash_fnv64_1a(data, 4, 0);
    printf("result4 %zu\n", result);
}

static void test_clib_hash_fnv_from_string() {
    size_t result = clib_hash_fnv32_1_from_string("hello", 0);
    printf("result1 %zu\n", result);

    result = clib_hash_fnv32_1a_from_string("hello", 0);
    printf("result2 %zu\n", result);

    result = clib_hash_fnv64_1_from_string("hello", 0);
    printf("result3 %zu\n", result);

    result = clib_hash_fnv64_1a_from_string("hello", 0);
    printf("result4 %zu\n", result);
}


static void test_clib_hash_fnv_main() {
    test_clib_hash_fnv();
    test_clib_hash_fnv_from_string();
}