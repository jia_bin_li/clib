
#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/crc/clib_hash_crc32.h"

static void test_crc32_be() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };

    clib_uint32_t result = clib_hash_crc32_be(data, 4, 0);
    printf("result %u\n",result);
}


static void test_crc32_be_string() {
    char * data = "1234";

    clib_uint32_t result = clib_hash_crc32_from_string_le(data, 0);
    printf("result %u\n",result);

    clib_uint32_t result2 = clib_hash_crc32_from_string_be(data, 0);
    printf("result2 %u\n",result2);
}



static void crc32_test_main(){
    test_crc32_be_string();


}