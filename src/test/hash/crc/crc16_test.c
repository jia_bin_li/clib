#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/crc/clib_hash_crc16.h"


static void test_clib_hash_crc16_ccitt() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    clib_uint32_t result = clib_hash_crc16_ccitt(data, 4, 0xffff);
    printf("result %u\n", result);
}


static void test_clib_hash_crc16_main(){
    test_clib_hash_crc16_ccitt();
}