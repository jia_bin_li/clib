#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/crc/clib_hash_crc8.h"


static void test_clib_hash_crc8_ccitt() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    clib_uint8_t result = clib_hash_crc8_ansi(data,4,0);
    printf("result %u\n", result);
}


static void test_clib_hash_crc8_main(){
    test_clib_hash_crc8_ccitt();
}