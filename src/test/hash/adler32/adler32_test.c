#include <stdio.h>
#include "../../../base/clib_base.h"
#include "../../../hash/adler32/clib_hash_adler32.h"


static void test_clib_hash_adler32() {
    clib_uint8_t data[] = {
            0x01, 0x02, 0x03, 0x04
    };
    clib_uint32_t result = clib_hash_adler32(data,4,0);
    printf("result %u\n", result);
}


static void test_clib_hash_adler32_main(){
    test_clib_hash_adler32();
}
