#include <stdio.h>
#include "../../random/num/clib_random_clock.h"
#include "../../random/uuid/clib_random_uuid.h"

static void random_test() {
    for (int i = 0; i < 10; ++i) {
        //获取时钟随机值
        clib_int64_t random = clib_random_clock();
        printf("random %lld\n", random);
    }

    for (int i = 0; i < 100; ++i) {
        //获取时钟随机值
        clib_int64_t random = clib_random_clock_range(0, 100);
        printf("random %lld\n", random);
    }

    //获取UUID字符串
    char uuid_str[CLIB_RANDOM_UUID_STRING_LENGTH];
    if (CLIB_RES_OK != clib_random_uuid_to_string(uuid_str)) {
        return;
    }
    printf("uuid %s\n", uuid_str);
}


static void random_test_main() {
    random_test();
}