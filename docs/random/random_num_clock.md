# 随机函数

## 描述

提供根据系统时间获得随机数的实现

## 使用方式

```c
#include <clib/random/clib_random.h>
```

### 使用系统时间作为随机种子并获取随机数

```c
/**
 * 使用系统时间作为随机种子并获取随机数
 * 每次调用函数都会使用系统纳秒值作为种子重新获取随机值
 * @return
 */
clib_int64_t clib_random_clock();
```

### 使用系统时间作为随机种子,按照范围获取随机值 [begin, end)

```c
/**
 * 使用系统时间作为随机种子,按照范围获取随机值 [begin, end)
 * @param begin 开始值
 * @param end 结束值
 * @return
 */
clib_int64_t clib_random_clock_range(clib_int64_t begin, clib_int64_t end);
```