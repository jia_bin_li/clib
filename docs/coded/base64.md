# Base64编码

## 描述

提供Base64编码算法

## 使用方式

```c
#include <clib/coded/clib_coded.h>
```

## 数据结构

```c
//BASE64输出最小空间值
#define CLIB_CODED_BASE64_OUTPUT_MIN(in)  (((in) + 2) / 3 * 4 + 1)
```

## 函数

### 对指定数据进行base64编码

```c
/**
 * base64编码
 * @param in_data 输入数据
 * @param in_size 输入数据大小
 * @param out_data 输出数据
 * @param out_size 输出数据大小
 * @return 输出数据的真实大小
 */
size_t clib_coded_base64_encode(
    clib_uint8_t const *in_data,
    size_t in_size,
    char *out_data,
    size_t out_size);
```

### 对指定base64编码字符串进行解码

```c
/**
 * base64解码
 * @param in_data 输入数据
 * @param in_size 输入数据大小
 * @param out_data 输出数据
 * @param out_size 输出数据大小
 * @return 输出数据的真实大小
 */
size_t clib_coded_base64_decode(
    char const *in_data,
    size_t in_size,
    clib_uint8_t *out_data,
    size_t out_size);
```
