# blizzard

## 描述

提供blizzard哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### blizzard hash计算

```c
/**
 * blizzard hash 计算
 * @param data 数据
 * @param size 大小
 * @param seed 种子
 * @return
 */
size_t clib_hash_blizzard(
        clib_uint8_t const *data, 
        size_t size, size_t seed);
```

### blizzard hash计算 字符串

```c
/**
 * blizzard hash 计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
size_t clib_hash_blizzard_from_string(
        char const *string, 
        size_t seed);
```