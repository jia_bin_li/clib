# sdbm

## 描述

提供sdbm哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### sdbm hash计算

```c
/**
 * sdbm hash 计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
size_t clib_hash_sdbm(
        clib_uint8_t const *data, 
        size_t size, 
        size_t seed);
```

### sdbm hash计算 字符串

```c
/**
 * sdbm hash 计算字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
size_t clib_hash_sdbm_from_string(
        char const *string, 
        size_t seed);
```