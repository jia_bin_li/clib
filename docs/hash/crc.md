# crc

## 描述

提供crc哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### ANSI标准crc8 hash计算

```c
/**
 * ANSI标准crc8
 * @param data 数据
 * @param size 大小
 * @param seed 种子
 * @return
 */
clib_uint8_t clib_hash_crc8_ansi(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint8_t seed);
```

### ANSI标准crc8 计算字符串

```c
/**
 * ANSI标准crc8 计算字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint8_t clib_hash_crc8_ansi_from_string(
        char const *string, 
        clib_uint8_t seed);
```

### ANSI标准crc16 hash计算
```c
/**
 * ANSI标准crc16
 * @param data 数据
 * @param size 数据长度
 * @param seed 种子
 * @return
 */
clib_uint16_t clib_hash_crc16_ansi(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint16_t seed);
```


### ANSI标准crc16 字符串hash计算
```c
/**
 * ANSI标准crc16 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint16_t clib_hash_crc16_ansi_from_string(
        char const *string, 
        clib_uint16_t seed);
```

### CCITT标准crc16 hash计算
```c
/**
 * CCITT标准crc16
 * @param data 数据
 * @param size 大小
 * @param seed 种子
 * @return
 */
clib_uint16_t clib_hash_crc16_ccitt(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint16_t seed);
```

### CCITT标准crc16 字符串

```c
/**
 * CCITT标准crc16 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint16_t clib_hash_crc16_ccitt_from_string(
        char const *string, 
        clib_uint16_t seed);

```

### 大端模式计算CRC32 hash计算

```c
/**
 * 大端模式计算CRC32
 * @param data 数据
 * @param size 数据长度
 * @param seed 种子
 * @return 计算后的CRC值
 */
clib_uint32_t clib_hash_crc32_be(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint32_t seed);
```

### 大端模式计算字符串的CRC32 hash计算

```c
/**
 * 大端模式计算字符串的CRC32
 * @param string 字符串
 * @param seed 种子
 * @return 计算后的CRC值
 */
clib_uint32_t clib_hash_crc32_from_string_be(
        char const *string, 
        clib_uint32_t seed);
```

### 小端模式计算CRC32 hash计算

```c
/**
 * 小端模式计算CRC32
 * @param data 数据
 * @param size 数据长度
 * @param seed 种子
 * @return 计算后的CRC值
 */
clib_uint32_t clib_hash_crc32_le(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint32_t seed);
```

### 小端模式计算字符串的CRC32 hash计算

```c
/**
 * 小端模式计算字符串的CRC32
 * @param string 字符串
 * @param seed 种子
 * @return 计算后的CRC值
 */
clib_uint32_t clib_hash_crc32_from_string_le(
        char const *string, 
        clib_uint32_t seed);
```