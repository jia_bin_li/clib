# murmur

## 描述

提供murmur哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### murmur hash计算

```c
/**
 * murmur hash 计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
size_t clib_hash_murmur(
        clib_uint8_t const *data, 
        size_t size, 
        size_t seed);
```

### murmur hash计算 字符串

```c
/**
 * murmur hash 计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
size_t clib_hash_murmur_from_string(
        char const *string, 
        size_t seed);
```