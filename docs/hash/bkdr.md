# bkdr

## 描述

提供bkdr哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### bkdr hash计算

```c
/**
 * bkdr hash计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
size_t clib_hash_bkdr(
        clib_uint8_t const *data, 
        size_t size, 
        size_t seed);
```

### bkdr hash计算 字符串

```c
/**
 * bkdr hash计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
size_t clib_hash_bkdr_from_string(
        char const *string, 
        size_t seed);
```