# adler32

## 描述

提供adler32哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### adler32 hash计算

```c
/**
 * adler32 hash计算
 * @param data 数据
 * @param size 数据长度
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_adler32(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint32_t seed);
```

### adler32 hash计算 字符串

```c
/**
 * adler32 hash计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_adler32_from_string(
        char const *string, 
        clib_uint32_t seed);
```