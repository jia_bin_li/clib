# md5

## 描述

提供md5哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### 简单一次性计算md5值

```c
/**
 * 简单一次性计算md5值
 * @param data 数据
 * @param size 数据大小
 * @param out_data 计算后的数据
 * @return
 */
int clib_hash_md5_simple(
        clib_uint8_t const *in_data, 
        size_t in_size, 
        clib_uint8_t *out_data);
```

### MD5初始化

```c
/**
 * MD5初始化
 * @param md5 md5结构体
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_hash_md5_init(struct clib_hash_md5 *md5);
```

### MD5 更新

```c
/**
 * MD5 更新
 * @param md5 md5结构体
 * @param data 数据
 * @param size 数据大小
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_hash_md5_update(
        struct clib_hash_md5 *md5,
        clib_uint8_t const *data, 
        size_t size);
```

### MD5 更新

```c
/**
 * MD5 生成
 * @param md5 md5结构体
 * @param data 生成的数据 需要16个字节大小
 * @return
 * 成功 0
 * 失败 非0
 */
int clib_hash_md5_final(
        struct clib_hash_md5 *md5, 
        clib_uint8_t *data);
```

