# djb2

## 描述

提供djb2哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### djb2 hash计算

```c
/**
 * djb2 hash计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
size_t clib_hash_djb2(
        clib_uint8_t const *data, 
        size_t size, 
        size_t seed);
```

### djb2 hash计算 字符串

```c
/**
 * djb2 hash计算 字符串计算
 * @param string 字符串
 * @param seed 种子
 * @return
 */
size_t clib_hash_djb2_from_string(
        char const *string, 
        size_t seed);
```