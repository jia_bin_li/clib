# fnv

## 描述

提供fnv哈希算法的实现

## 使用方式

```c
#include <clib/hash/clib_hash.h>
```

### fnv32_1 hash 计算

```c
/**
 * fnv32_1 hash 计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_fnv32_1(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint32_t seed);
```

### fnv32_1 hash计算 字符串

```c
/**
 * fnv32_1 hash 计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_fnv32_1_from_string(
        char const *string, 
        clib_uint32_t seed);
```

### fnv32_1a hash计算

```c
/**
 * fnv32_1a hash 计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_fnv32_1a(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint32_t seed);
```


### fnv32_1a hash计算 字符串

```c
/**
 * fnv32_1a hash计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint32_t clib_hash_fnv32_1a_from_string(
        char const *string, 
        clib_uint32_t seed);
```


### fnv64_1 hash计算

```c
/**
 * fnv64_1 hash计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
clib_uint64_t clib_hash_fnv64_1(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint64_t seed);
```

### fnv64_1 hash计算 字符串

```c
/**
 * fnv64_1 hash计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint64_t clib_hash_fnv64_1_from_string(
        char const *string, 
        clib_uint64_t seed);
```

### fnv64_1a hash计算

```c
/**
 * fnv64_1a hash计算
 * @param data 数据
 * @param size 数据大小
 * @param seed 种子
 * @return
 */
clib_uint64_t clib_hash_fnv64_1a(
        clib_uint8_t const *data, 
        size_t size, 
        clib_uint64_t seed);
```


### fnv64_1a hash计算 字符串

```c
/**
 * fnv64_1a hash计算 字符串
 * @param string 字符串
 * @param seed 种子
 * @return
 */
clib_uint64_t clib_hash_fnv64_1a_from_string(
        char const *string, 
        clib_uint64_t seed);
```